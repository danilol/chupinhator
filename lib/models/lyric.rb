class Chupinhator::Lyric
  include Mongoid::Document

  embedded_in :artist, class_name: "Chupinhator::Artist"

  field :imported_id
  field :desc
  field :url
  field :lyric
  field :lyric_text
end
