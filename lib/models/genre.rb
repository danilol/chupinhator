class Chupinhator::Genre
  include Mongoid::Document

  embedded_in :artist, class_name: 'Chupinhator::Artist'

  field :name
  field :url
end
