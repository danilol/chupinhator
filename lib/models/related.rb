class Chupinhator::Related
  include Mongoid::Document

  embedded_in :artist, class_name: "Chupinhator::Artist"

  field :imported_id
  field :name
  field :url
end
