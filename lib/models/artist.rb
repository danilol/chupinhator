class Chupinhator::Artist
  include Mongoid::Document
  include Mongoid::Timestamps

  embeds_many :genres,  class_name: "Chupinhator::Genre"
  embeds_many :related, class_name: "Chupinhator::Related"
  embeds_many :lyrics,  class_name: "Chupinhator::Lyric"
  embeds_many :albums,  class_name: "Chupinhator::Album"

  field :imported_id
  field :desc
  field :url
  field :pic_small
  field :pic_medium
end
