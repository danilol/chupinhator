module Chupinhator::ContentImporter
  class << self
    def get_all
      response = JSON.parse(HTTParty.get(content_url))
      build_object(response["artist"])
    end

    def content_url
      "#{SOURCE["base_url"]}/#{SOURCE["artist"]}/#{SOURCE["index"]}"
    end

    def build_object(artist)
      new_artist = build_artist(artist)

      albums = build_album(artist["albums"]["item"])
      genres = build_genres(artist["genre"])
      lyrics = build_lyrics(artist["lyrics"]["item"])
      relateds = build_relateds(artist["related"])


      new_artist.albums = albums
      new_artist.genres = genres
      new_artist.lyrics = lyrics
      new_artist.related = relateds

      new_artist.save
    end

    def build_album(albums)
      new_albums = []

      albums.each do |album|
        new_album = Chupinhator::Album.new
        new_album.imported_id = album["id"]
        new_album.desc        = album["desc"]
        new_album.url         = album["url"]
        new_album.year        = album["year"]

        new_albums << new_album
      end

      new_albums
    end

    def build_genres(genres)
      new_genres = []

      genres.each do |genre|
        new_genre = Chupinhator::Genre.new
        new_genre.name        = genre["name"]
        new_genre.url         = genre["url"]

        new_genres << new_genre
      end

      new_genres
    end

    def build_lyrics(lyrics)
      new_lyrics = []

      lyrics.each do |lyric|
        new_lyric = Chupinhator::Lyric.new
        new_lyric.imported_id = lyric["id"]
        new_lyric.desc        = lyric["desc"]
        new_lyric.url         = lyric["url"]
        new_lyric.lyric       = lyric["url"].gsub("\/matanza\/", "").gsub(".html", "")
        new_lyric.lyric_text  = get_lyric_text(new_lyric.lyric)

        puts new_lyric.lyric

        new_lyrics << new_lyric
      end

      new_lyrics
    end

    def build_relateds(relateds)
      new_relateds = []

      relateds.each do |related|
        new_related = Chupinhator::Related.new
        new_related.imported_id = related["id"]
        new_related.name        = related["name"]
        new_related.url         = related["url"]

        new_relateds << new_related
      end

      new_relateds
    end

    def build_artist(artist)
      new_artist = Chupinhator::Artist.new
      new_artist.imported_id = artist["id"]
      new_artist.desc        = artist["desc"]
      new_artist.url         = artist["url"]
      new_artist.pic_small   = artist["pic_small"]
      new_artist.pic_medium  = artist["pic_medium"]

      puts new_artist.desc

      new_artist
    end

    def get_lyric_text(lyric)
      url = "http://api.vagalume.com.br/search.php?art=#{SOURCE["artist"]}&mus=#{lyric}"
      response = HTTParty.get(url)
      response["mus"].first["text"]
    end
  end
end
