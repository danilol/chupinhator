class Chupinhator::Album
  include Mongoid::Document

  embedded_in :artist, class_name: "Chupinhator::Artist"

  field :imported_id
  field :desc
  field :url
  field :year
end
