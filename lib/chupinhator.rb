require "chupinhator/version"
require "httparty"

ENV['RACK_ENV'] ||= 'development'

root = File.expand_path(File.dirname(__FILE__))
$: << root
$: << File.expand_path(File.join(File.dirname(__FILE__), '..'))

require 'bundler'
Bundler.require

require 'config/environment'

SOURCE = YAML.load_file("./config/source.yml")["source"]

require 'lib/content_importer'
require 'lib/models/related'
require 'lib/models/genre'
require 'lib/models/lyric'
require 'lib/models/album'
require 'lib/models/artist'
