# Chupinhator

WIP - Tool to get all lyrics from desired bands.

Soon: Interface to execute the procedure
      Option to save to a file (currently saving on Mongodb)

## Usage

 - Config the band on config/source.yml:

  source:
    base_url: http://www.vagalume.com.br
    artists:
     - bad-religion
    index:    index.js
    output:   file

- Run:

bundle console

Chupinhator::ContentImporter.get_all


